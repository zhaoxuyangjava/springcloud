package com.cangoonline.auth;

import com.cangoonline.common.utils.IdWorker;
import com.cangoonline.common.utils.JwtUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by shaohaojie on 2020/3/13.
 */
@EnableEurekaClient
@SpringBootApplication
@EntityScan("com.cangoonline.common.model")//扫描实体类
@ComponentScan(basePackages={"com.cangoonline.auth", "com.cangoonline.common"})//扫描本项目下的所有类
public class AuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthApplication.class);
    }

    @Bean
    public JwtUtils jwtUtils() {
        return new JwtUtils();
    }

    @Bean
    public IdWorker idWorker() {
        return new IdWorker();
    }
}
