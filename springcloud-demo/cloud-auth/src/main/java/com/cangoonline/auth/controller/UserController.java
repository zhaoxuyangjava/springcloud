package com.cangoonline.auth.controller;

import com.cangoonline.common.api.UserControllerApi;
import com.cangoonline.common.model.response.Result;
import com.cangoonline.common.model.response.ResultCode;
import com.cangoonline.common.utils.IdWorker;
import com.cangoonline.common.utils.JwtUtils;
import com.cangoonline.common.web.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

import static javafx.scene.input.KeyCode.H;

/**
 * Created by shaohaojie on 2020/3/13.
 */
@RestController
@CrossOrigin
@RequestMapping("/auth")
public class UserController extends BaseController implements UserControllerApi {

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private IdWorker idWorker;

    @Override
    @PostMapping("/login")
    public Result<String> login(String username, String password) throws Exception {
        Map<String, Object> map = new HashMap<>();
        map.put("username", username);
        map.put("password", password);
        String jwtToken = jwtUtils.createJwt(idWorker.nextId() + "", username, map);
        return new Result<>(ResultCode.SUCCESS, jwtToken);
    }
}
