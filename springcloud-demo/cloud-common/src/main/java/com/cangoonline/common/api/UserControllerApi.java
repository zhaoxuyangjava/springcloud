package com.cangoonline.common.api;

import com.cangoonline.common.model.response.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * Created by shaohaojie on 2020/3/12.
 */
@Api(value="认证服务", description = "测试认证接口")
public interface UserControllerApi {

    @ApiOperation("用户登录接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户名", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "password", value = "密码", required = true, dataType = "String", paramType = "query")
    })
    Result login(String username, String password) throws Exception;
}
