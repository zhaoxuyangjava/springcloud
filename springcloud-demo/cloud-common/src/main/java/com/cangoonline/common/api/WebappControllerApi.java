package com.cangoonline.common.api;

import com.cangoonline.common.model.contract.ContractBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import java.util.List;

/**
 * Created by shaohaojie on 2020/3/12.
 */
@Api(value="web接口", description = "测试web接口")
public interface WebappControllerApi {

    @ApiOperation("查询合同接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", required = true, dataType = "int", paramType = "path"),
            @ApiImplicitParam(name = "size", value = "每页记录数", required = true, dataType = "int", paramType = "path")
    })
    List<ContractBean> getContract(int page, int size) throws Exception;
}
