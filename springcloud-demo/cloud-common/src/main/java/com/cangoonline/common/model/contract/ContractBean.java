package com.cangoonline.common.model.contract;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by shaohaojie on 2020/3/12.
 */
@Table(name="t_b_contract")
public class ContractBean implements Serializable {
    private static final long serialVersionUID = -1259282062469561470L;

    @Id
    private String id;
    @Column(name = "applyCd")
    private String applyCd;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApplyCd() {
        return applyCd;
    }

    public void setApplyCd(String applyCd) {
        this.applyCd = applyCd;
    }
}
