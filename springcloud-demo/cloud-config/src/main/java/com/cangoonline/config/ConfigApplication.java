package com.cangoonline.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by shaohaojie on 2020/3/10.
 */

@EnableEurekaClient
@EnableConfigServer
@SpringBootApplication
@ComponentScan(basePackages={"com.cangoonline.config"})//扫描本项目下的所有类
public class ConfigApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConfigApplication.class);
    }
}
