package com.cangoonline.file;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by shaohaojie on 2020/3/3.
 */
@EnableEurekaClient
@SpringBootApplication
//@EntityScan("com.cangoonline.file")//扫描实体类
@ComponentScan(basePackages={"com.cangoonline.file"})//扫描本项目下的所有类
public class FileApplication {
    public static void main(String[] args) {
        SpringApplication.run(FileApplication.class);
    }
}
