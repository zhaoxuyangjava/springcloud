package com.cangoonline.webapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * Created by shaohaojie on 2020/3/3.
 */
@EnableEurekaClient
@EnableFeignClients
@SpringBootApplication
@EntityScan("com.cangoonline.common.model")//扫描实体类
@ComponentScan(basePackages={"com.cangoonline.webapp", "com.cangoonline.common"})//扫描本项目下的所有类
@MapperScan(basePackages = {"com.cangoonline.webapp.dao"})
public class WebappApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebappApplication.class);
    }
}
