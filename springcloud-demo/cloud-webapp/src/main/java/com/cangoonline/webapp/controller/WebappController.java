package com.cangoonline.webapp.controller;

import com.cangoonline.common.api.WebappControllerApi;
import com.cangoonline.common.model.contract.ContractBean;
import com.cangoonline.common.web.BaseController;
import com.cangoonline.webapp.feign.IFileService;
import com.cangoonline.webapp.service.IContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by shaohaojie on 2020/3/3.
 */
@RestController
@CrossOrigin
@RequestMapping("/webapp")
@RefreshScope
public class WebappController extends BaseController implements WebappControllerApi {

    @Autowired
    private IFileService fileService;

    @Autowired
    private IContractService contractService;

    @Value("${user.name}")
    private String name;

    @RequestMapping(value = "/getMsg", method = RequestMethod.GET)
    public String getMsg(String name) {
        String str = fileService.testFile();
        return "web服务\n" +
                "请求参数：" + name +
                "\nFeign远程调用" + str;
    }

    @RequestMapping(value = "/getContract/{page}/{size}", method = RequestMethod.GET)
    public List<ContractBean> getContract(@PathVariable("page") int page, @PathVariable("size") int size) throws Exception {
        return contractService.findAll(page, size);
    }
}
