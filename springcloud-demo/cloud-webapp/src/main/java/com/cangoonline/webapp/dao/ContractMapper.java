package com.cangoonline.webapp.dao;

import com.cangoonline.common.model.contract.ContractBean;
import tk.mybatis.mapper.common.Mapper;

/**
 * Created by shaohaojie on 2020/3/12.
 */
public interface ContractMapper extends Mapper<ContractBean> {
}
