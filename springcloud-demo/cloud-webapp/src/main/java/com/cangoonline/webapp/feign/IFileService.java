package com.cangoonline.webapp.feign;

import com.cangoonline.webapp.feign.hystrix.FeignHystrix;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * Created by shaohaojie on 2020/3/5.
 */
@FeignClient(value = "cloud-file", fallback = FeignHystrix.class)
public interface IFileService {

    @PostMapping("/file/test")
    public String testFile();
}
