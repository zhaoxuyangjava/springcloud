package com.cangoonline.webapp.feign.hystrix;

import com.cangoonline.webapp.feign.IFileService;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by shaohaojie on 2020/3/5.
 */
@Component
public class FeignHystrix implements IFileService {
    @Override
    public String testFile() {
        return "===========================熔断";
    }
}
