package com.cangoonline.webapp.service;

import com.cangoonline.common.model.contract.ContractBean;

import java.util.List;

/**
 * Created by shaohaojie on 2020/3/12.
 */
public interface IContractService {

    List<ContractBean> findAll(int page, int size) throws Exception;
}
