package com.cangoonline.webapp.service.impl;

import com.cangoonline.common.exception.CommonException;
import com.cangoonline.common.model.contract.ContractBean;
import com.cangoonline.common.model.response.ResultCode;
import com.cangoonline.webapp.dao.ContractMapper;
import com.cangoonline.webapp.service.IContractService;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by shaohaojie on 2020/3/12.
 */
@Service
public class ContractServiceImpl implements IContractService {

    @Autowired
    private ContractMapper contractMapper;


    @Override
    public List<ContractBean> findAll(int page, int size) throws Exception {
        if (page <= 0 || size <= 0) {
            throw new CommonException(ResultCode.PARAMETERNOTVALID);
        }
        PageHelper.startPage(page, size);
        return contractMapper.selectAll();
    }
}
