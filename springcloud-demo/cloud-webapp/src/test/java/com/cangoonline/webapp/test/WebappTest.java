package com.cangoonline.webapp.test;

import com.cangoonline.common.model.contract.ContractBean;
import com.cangoonline.webapp.service.IContractService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * Created by shaohaojie on 2020/3/13.
 */
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class WebappTest {

    @Autowired
    private IContractService contractService;

    @Test
    public void test1() throws Exception {
        List<ContractBean> all = contractService.findAll(1, 3);
        System.out.println(all.size());
    }
}
