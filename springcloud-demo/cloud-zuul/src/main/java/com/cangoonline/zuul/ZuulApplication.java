package com.cangoonline.zuul;

import com.cangoonline.common.utils.JwtUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by shaohaojie on 2020/3/3.
 */
@EnableEurekaClient
@EnableZuulProxy
@SpringBootApplication
@ComponentScan(basePackages={"com.cangoonline.zuul", "com.cangoonline.common"})//扫描本项目下的所有类
public class ZuulApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZuulApplication.class);
    }

    @Bean
    public JwtUtils jwtUtils() {
        return new JwtUtils();
    }
}
