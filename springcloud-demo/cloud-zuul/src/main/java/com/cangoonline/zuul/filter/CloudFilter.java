package com.cangoonline.zuul.filter;

import com.cangoonline.common.utils.JwtUtils;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by shaohaojie on 2020/3/3.
 */
@Component
public class CloudFilter extends ZuulFilter {

    @Autowired
    private JwtUtils jwtUtils;

    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        if (!request.getServletPath().contains("/auth/login")) {
            String authorization = request.getHeader("Authorization");
            System.out.println("=====================Authorization:" + authorization);
            if (StringUtils.isEmpty(authorization)) {
                ctx.setSendZuulResponse(false);
                ctx.setResponseStatusCode(401);
                ctx.set("isSuccess", false);
                ctx.setResponseBody("token is empty");
            }else {
                try {
                    Claims claims = jwtUtils.parseJwt(authorization);
                    ctx.setSendZuulResponse(true);
                    ctx.setResponseStatusCode(200);
                    ctx.set("isSuccess", true);
                } catch (Exception e) {
                    e.printStackTrace();
                    ctx.setSendZuulResponse(false);
                    ctx.setResponseStatusCode(401);
                    ctx.set("isSuccess", false);
                    ctx.setResponseBody("TOKEN IS UNAUTHENTICATED");
                }
            }
        }

        return null;
    }
}
